# Slow kata : Ralentir pour aller plus vite

## Introduction

### Qui on est ?

* Alexis Benoist
* Thomas Clavier

### Pratique répandue

 * Les adeptes du Taï Chi Chuan.
 * L'entraînement isométrique.
 * Maître Kuroda Tetsuzan s'entraine quasiment exclusivement lentement. Plus jeune à atteindre le 8ème dan au kendo à 20 ans, expert en sabre.
 * Le patinage de vitesse : qui est systématiquement sur le podium depuis 44 ans ? Ils ont entre 1 et 3 personnes tous les ans et quasiment dans toutes les épreuves. Les néherlandais, ils s'entrainent lentement ... Résultat d'une équipe de chercheur d'Amsterdam.

Origine de nos recherches: "slow debug d'/ut7", agile france

### Pourquoi ?

#### Pour les équipes ?

Parfaire son geste, maîtriser ses outils et sa technique, faire des choix d'équipe (standards, conventions, technologies) et se connaître, voir même apprendre à coder différemment.
Pour qu'un livre soit facile à lire, il faut qu'il y ai une unité de style, même s'il y a de nombreux intervenants. Or c'est important d'avoir du code lisible, un développeur passe 80% de son temps à lire du code et à le dérouler mentalement, pour trouver une définition de fonction ou de variable, comprendre un bug, etc. Donc c'est très important qu'une équipe décide de son style d'écriture.

#### Pour ici ?

* Pas de choix d'équipe, car on est pas une équipe.
* Coder ensemble et voir plusieurs manières de penser, vous allez peut-être même voir des réactions de participants que vous observez chez vos collègues, mais là vous allez les comprendre.
* Prendre du recul sur sa propre façon de penser.
* Découvrir ensemble le mode de fonctionnement du ralentir (poser les options, discuter, décider, comment décider, et voir que l'on est surpris)

### Déroulement de la session

Le but c'est de pratiquer pas de finir le problème.
On ne juge pas la pratique comme en méditation et le but n'est pas de finir, c'est de s'entrainer pour faire du méta. S'observer et améliorer le geste. Théoriser et simplifier. C'est en simplifiant votre geste que vous allez arriver à plus de sérénité (livrer plus vite, plus efficacement et sans stress)

On va coder en commençant par spécifier ce à quoi on s'attend : C'est du TDD.

Agenda :
 - Je serais le clavier de la session et Thomas l'animateur
 - Rendez-vous pour le débriefing à <Heure>
 - Attendez-vous à être surpris !

=> Thomas :
- Le problème
- présenter l’intérêt du clavier : les décharger de la complexité du language formel.

## Conclusion / Débriefing

- Qu'avez-vous appris ?
- Qu'es-ce qui vous à surpris ?

## Tweets

* Atelier d'écriture ouvert à tous ! Le code, la littérature contemporaine.
* Atelier slow mob programming pour pratiquer le méta et comprendre des choses que vous n'avez jamais vues avant !

http://2018.agiletour-lille.org/conferences/#58

## Pourquoi s'entrainer à coder ?

* Parfaire son geste, maitriser ses outils et sa technique pour être capable de faire les bons choix en compétition, enfin devant le code de prod.
* S'entrainer avec du code à l'algorithmique très simple pour tester de nombreuses techniques (paterne, langages, IDE, etc.)
* Comme dans les arts martiaux, d'ailleurs on empreinte le terme à ce domaine.

## Pourquoi lentement

- la lecture de http://www.leotamaki.com/2016/03/eloge-de-la-lenteur-dans-la-pratique-martiale.html
- patinage de vitesse :
  * http://vazel.blog.lemonde.fr/2014/02/11/pour-patiner-vite-il-faut-sentrainer-lentement/
  * https://www.ncbi.nlm.nih.gov/pubmed/24408352

## Dérouler

* Arbre d'exploration des possibles : noter chaque idée sur un post-it
* Quel petite action voulez-vous faire ?
* Quel est le résultat espéré ?
  * Si c'est un test, quel sera l'impact sur le code ? Celà correspond à un tout petit pas ?
* Faire l'action
* Observer

# Notes post-conf
- Pourquoi pas renommer en "Atelier d'écriture ... de code"
- faire noter les apprentissages, en commun ou en perso ?
- le clavier note sur des postit les apprentissages perçu.
- prévoir un système pour la prise de parole dans la salle (baton de parole ? main levée)
- un truc de conclusion, un support à emporter.
- Vérifier les chiffres sur le nombre de lignes de code vs nombre de ligne oeuvre litéraire
- rester au niveau des envies vs action d'implem / tech pour garder un public plus large.
- expliquer les règles métiers avec le cas générale ... une slide ?
- modifier le pitch de l'atelier pour dire que même les non-dev vont écrire du code
