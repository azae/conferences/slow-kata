KEEP=.svg|.tex|Makefile|.potx|.odp|.gitlab-ci.yml|.gitignore|README.md
SVGFILES = $(wildcard *.svg)
TEXFILES = $(wildcard *.tex)

%.pdf: %.svg
	inkscape --export-area-page --file=$< --export-pdf=$@

%.pdf: %.tex
	pdflatex  $<
	pdflatex  $<

pdf: $(patsubst %.svg,%.pdf,$(SVGFILES)) $(patsubst %.tex,%.pdf,$(TEXFILES))
